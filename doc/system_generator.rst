System generator
#####################

cell-gen.py
***********

This python scripts is written to generate bulk cells with the use of different
bravais lattice as input. Actually, it is written only for maximum two
different atoms in the fcc structure.

Features
========

* Available structures:

  * fcc c 


* Available output format:

  * XYZ

  * cry (fractional coordinates)

What do you need to run it
==========================

  You have to use those versions or later ones to run the program
  correctly.

  * Python 3.7.4

    * module numpy version 1.17.2

How to run it
=============

For this software, you do not need input files. All the settings are done at
the beginning of the program. Just launch the program when the input part is
correct for your case.

Inputs
=======

.. note::
  The type of input is written as *str* for a string, *flt* for a
  float and *int* for an integer

:atnam1, atnam2: *(str)* Corresponds to the label of respectively atom 1 and
  atom 2. If both are the same, just put the same name for each parameter.The
  atom located in 000 will be atom 1.

:resdirname: *(str)* Indicate the path of the result directory.

:resfilename: *(str)* Corresponds to the name of the output file. It has to be
  written without the extension

:outform: *(str)* Can be xyz or cry. They are detailed below.

  * xyz: corresponds to an XYZ output format

  * cry: corresponds to an output format with fractional coordinates

:bravaislat: *(str)* Corresponds to the wanted bravais lattice. The one which
  is available is written below:

  * fcc: face centred cubic structure

:a: *(flt)* Corresponds to the lattice parameter along the a cell vector. The
  unit is Angstrom

  * fcc lattice: only the a parameter is sufficient to build a fcc structure

:nlayer: *(int)* Corresponds to the number of layers

:nrepa, nrepb: *(int)* Corresponds to the number of repetition along the a and b
  cell vectors respectively

.. warning::
  nlayer, nrepa and nrepb can not be 0

.. warning::
  A repetition is considered as a translation from one atom to another one. For
  example, to build the standard NaCl structure, you have to set these parameters
  to 2 and nlat to 3.

Here is a full example of the input part of cell-gen.py:

.. code-block:: python
  :linenos:

  # Atoms label. The first one will occpupied the 0,0,0 position.
  atnam1 = "Na"
  atnam2 = "Cl"
  # Path of the result directory
  resdirname = "path/to/result/directory"
  # Name of the resultfile (without the extension)
  resfilename = "NaCl001"
  # Output format (xyz,cry)
  outform = "xyz"
  # Bravais lattice (fcc)
  bravaislat = "fcc"
  # Lattice parameter in Å
  a = 5.6402
  # Number of layers (1). Standard lattice: 3
  nlayer = 3
  # Number of repetion along the a and b direction
  # 2 in each direction corresponds to the common reprezentation for a fcc
  # geometry.
  nrepa = 2
  nrepb = 2
