vc-relax calculations
#####################

vc-relax.py
***********

This python script is written in order to extract information from vc-relax
calculations as the lattice parameter evolution for example.

Features
========

* Available structures:

  * fcc c

  * fcc p with a translation of 45º for a and b vectors

* Evolution of the lattice parameter for each file
* Evolution of the lattice parameter versus the cut-off
  energy for a set of files


What do you need to run it
==========================

You have to use those versions or later ones to run the program
correctly.

* Python 3.7.4

  * module re version 2.2.1

  * module numpy version 1.17.2


How to run it
=============

You have to put the set of output files into the same directory.
Each filename has to have similar construction. The structure
has to be: [cut-off energy].out. All the energy values have to
be written with the same number of character. This allows the program to sort correctly all the
filenames at the beginning.


Examples:

* 110.out
* 050.out

The program needs functions which are located into the qefunction.py
file. As a consequence, you have to put both programs into the same
directory.
You have to change the input part of the program, located at the
beginning.

Inputs
=======

.. note::
  The type of input is written as *str* for a string, *flt* for a
  float and *int* for an integer

:filepath:
  (*str*) Indicate the path of the input directory. The input
  directory is the one containing the set off *.out*
  files

:resultpath: (*str*) Indicate the path of the output directory. This is the directory in which
  all the result files (*.res*) will be saved

:structure: (*int*) Indicate the structure of the computed structure. The
  values are detailed later in this section

:outfilename1: (*str*) Corresponds to the name of the result file containing
  the evolution of the lattice parameter according to the cut-off energy.
  The name has to be written without spaces and within the extension ".res"
  at the end.

:allatcal: (*int*) Can take the value 0 or 1. If set to 1, the software will
  compute the evolution of the lattice parameter for each step for each file.
  If set to 0, those calculations

:outfilename2: (*str*) Corresponds to the name of the result files containing
  the evolution of the lattice parameter according to the number of steps for
  only one calculation. The extension ".res" does not have to be written at
  the end. The value of the cut-off energy will appears just after this string
  You will have as much result files as input files

Here is a full example of the input part of vc-relax.py:

.. code-block:: python
  :linenos:

  filepath = "path_to_the_input_directory"
  resultpath = "path_to_the_result_directory"
  structure = 0       #"classical" fcc structure
  outfilename1 = "ecut_vs_lat.res"
  alllatcal = 1       #computation of lattice vs steps
  outfilename2 = "alllattice_Ecut_"
  #Will gives 'alllattice_Ecut_100.res' as an example

.. warning::
  All the input variables have to be filed. If not, the program will
  stop
