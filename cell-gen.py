#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This python scripts is written to generate cells with the use of different
bravais lattice as input. It is written only for two atoms in the fcc structure
"""

print('------------------------------------\n')

"""
INPUTS -------------------------------------------------------------------------
"""
# Atoms label. The first one will occpupied the 0,0,0 position.
atnam1 = "Na"
atnam2 = "Cl"
# Path of the result directory
resdirname = "path_to/output/directory"
# Name of the resultfile (without the extension)
resfilename = "outputfilename"
# Output format (xyz,cry)
outform = "xyz"
# Bravais lattice (fcc)
bravaislat = "fcc"
# Lattice parameter in Å
a = 5.6402
# Number of layers (1). Standard lattice: 3
nlayer = 2
# Number of repetion along the a and b direction
# 2 in each direction corresponds to the common reprezentation for a fcc
#  geometry.
nrepa = 2
nrepb = 2

"""
END OF THE INPUT PART-----------------------------------------------------------
"""

"""
MODULES
"""

import os
import numpy as np

"""
FUNCTIONS
"""

import qefunction as qe

def outformchoice(matrix,latparam,outform,resfilename):
    """This function set the output format
    """
    if outform == "xyz":
        print('The choosen output format is xyz')
        form = outxyz(matrix)
    elif outform == "cry":
        print('The choosen output format is cry')
        print('The lattice parameter is: {0:7.5f}'.format(latparam))
        form = outcrys(matrix,latparam)
    else: #Stopping the program if the outfile format does not exist
        print("The output format {0} does not exist in this program".format(outform))
        print("The program has to stop")
        print('\n---------------------------------END')
        exit()

def outxyz(matrix):
    """This function write a cartesian matrix into a xyz file format
    """
    outfilename = resfilename + "." + "xyz"
    print('The system is saved as {}'.format(outfilename))
    nbatoms = (1+nrepa)*(1+nrepb)*nlayer
    outfile = open(outfilename,'w')
    outfile.write('{0:d}\n'.format(nbatoms))
    outfile.write('Commentary\n')
    for i in range(len(matrix)):
        if (i % 2 ==0):
            outfile.write('{0:6.2} {1:16.12f} {2:16.12f} {3:16.12f} \n'.format(
                matrixcart[i][0], matrixcart[i][1], matrixcart[i][2], matrixcart[i][3]))
    for i in range (len(matrix)):
        if (i % 2 !=0):
            outfile.write('{0:6.2} {1:16.12f} {2:16.12f} {3:16.12f} \n'.format(
                matrixcart[i][0], matrixcart[i][1], matrixcart[i][2], matrixcart[i][3]))
    outfile.write('\n')
    outfile.close()

def outcrys(matrix,latparam):
    """This function write a cartesian matrix into a crystal format
    """
    outfilename = resfilename + "." + "cry"
    print('The system is saved as {}'.format(outfilename))
    nbatoms = (1+nrepa)*(1+nrepb)*nlayer
    outfile = open(outfilename,'w')
    outfile.write('Lattice parameter: {0:10.7f} Angstroms \n'.format(latparam))
    latparambohr = angstobohr(latparam)
    outfile.write('Lattice parameter: {0:10.7f} Bohr \n\n'.format(latparambohr))
    xmax = nrepa * 0.5 * a
    ymax = nrepb * 0.5 * a
    zmax = nlayer * 0.5 * a
    for i in range(len(matrix)):
        if (matrix[i][1]==xmax) or (matrix[i][2]==ymax) or (matrix[i][3]==zmax):
            pass
        else:
            if (i % 2 ==0):
                outfile.write('{0:6.2} {1:16.12f} {2:16.12f} {3:16.12f} \n'.format(
                    matrix[i][0], matrix[i][1]/xmax, matrix[i][2]/ymax, matrix[i][3]/zmax))
        #
    for i in range(len(matrix)):
        if (matrix[i][1]==xmax) or (matrix[i][2]==ymax) or (matrix[i][3]==zmax):
            pass
        else:
            if (i % 2 !=0):
                outfile.write('{0:6.2} {1:16.12f} {2:16.12f} {3:16.12f} \n'.format(
                    matrix[i][0], matrix[i][1]/xmax, matrix[i][2]/ymax, matrix[i][3]/zmax))
    outfile.close()

"""
MAIN
"""

os.chdir(resdirname)   # Going to the result directory
print('The result directory is: {0}\n'.format(resdirname)) #Print the information
matrixcart = qe.ibrav(bravaislat,a,nrepa,nrepb,nlayer,atnam1,atnam2) # Cartesian matrix constuction
# Print the cartezian matrix
print('\nThe cartesian matrix is:\n {}\n'.format(matrixcart))
# Write the results in a file in the choosen format
outformchoice(matrixcart,a,outform,resfilename)

print('\n---------------------------------END')
