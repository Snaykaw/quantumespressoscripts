[![Documentation Status](https://readthedocs.org/projects/quantumespressoscriptsmi/badge/?version=latest)](https://quantumespressoscriptsmi.readthedocs.io/en/latest/?badge=latest)

# QuantumEspressoScripts

All those scripts are written in order to treat data from Quantum Espresso software.

## vc-relax calculations

### vc-relax<span></span>.py

This python script is written in order to extract information from vc-relax calculations as the lattice parameter evolution for example.

#### Features

* Available structures:
  * fcc c
  * fcc p (with a rotation of 45º for a and b vectors)
* Evolution of the lattice parameter for each file
* Evolution of the lattice parameter versus the cut-off energy for a set of files

## system generator

This python scripts is written to generate bulk cells with the use of different bravais lattice as input.
Actually, it is written only for maximum two different atoms in the fcc structure. 

### cell-gen<span></span>.py

#### Features

* Available structure:
  * fcc c
* Available outputs:
  * XYZ
  * cry (fractional coordinates)
