#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This python scripts is written to reat data from quantum espresso
   vc-relax calculations"""

"""
INPUTS -------------------------------------------------------------------------
"""
# INPUT DIRECTORY PATH
filepath = "path/to/input/directory"
# RESULT DIRECTORY PATH
resultpath = "path/to/output/directory" 
# STRUCTURE
# fcc -> 0
# fcc (p2sqrt2_2sqrt2_1) -> 1
structure = 0
# OUTPUT NAMES
# evolution of the lattice parameter according to ecut
outfilename1 = "name_of_the_outfile.res"
# evolution of the lattice parameter according to the number of step of The
# vc-relax calculation
allatcal = 0 #calc lattice parameter vs step ? (yes = 1/ no = 0)
outfilename2 = "Name_of_the_outfile_without_extension" # Name of the outfile
# corresponding to the lattice parameter evolution according to steps
"""
END OF THE INPUT PART-----------------------------------------------------------
"""

"""
MODULES
"""

import sys
import os
import re
import numpy as np
import math

"""
FUNCTIONS
"""

import qefunction as qe

print('------------------------------------\n')
os.chdir(filepath)

"""
MAIN
"""
# List of files in the PATH directory
filelist = qe.files(filepath)


# Initialization
final_lat_param = []
ecut_list = []

# Printing information
if structure==0:
    print('The cell parameter is computed with a (fcc) lattice !\n')
elif structure==1:
    print('The cell parameter is computed with a p2sqrt(2)2sqrt(2)1 (fcc) lattice !\n')
# BEGIN PARSING ALL .OUT FILES FROM PATH
for i in range(len(filelist)):
    print('Parsing {0} file ...'.format(str(filelist[i])))
    # Going into the input directory
    os.chdir(filepath)
    # Opening an input file
    outfile = open(filelist[i],"r")
    # Structure choice and calculation of the lattice parameters
    # with a conversion bohr to angstrom
    if structure==0: #fcc structure
        matrix = qe.patternline3_fcc(outfile,"CELL_PARAMETERS")
        # Conversion into angstroms
        matrix = qe.bohrtoangs(matrix)
    elif structure==1: #2psqrt2 structure
        matrix = qe.patternline3_2psqrt2(outfile,"CELL_PARAMETERS")
    # Closing input file
    outfile.close()
    #WRITTING RESULTS FILES
    # Going into results directory
    os.chdir(resultpath)
    # Creating the name of the result file
    resfilename = filelist[i].split(".")[0] + ".res"
    #resfilename = qe.nameresfile(filelist[i],outfilename2,nsep=1)
    # Writting the coordinates as iteration number, a, b, c in 3 rows
    if allatcal ==1:
        # Creating or reinitialize the result file
        resfile_alllatice = open(resfilename,"w")
        for j in range(len(matrix[0])):
            resfile_alllatice.write('{0:3d} {1:20.9f} {2:20.9f} {3:20.9f} \n'.format(j+1, float(matrix[0][j]), float(matrix[1][j]), float(matrix[2][j])))
        resfile_alllatice.close()
    # Saving final lattice parameter into an array
    ecut_list.append(int(filelist[i].split(".")[0]))
    #ecut_list.append(int(qe.valuefromfilename(filelist[i],separator="_",nsep=2)))
    final_lat_param.append(float(matrix[2][-1]))

# Writting the lattice parameter according to time files
os.chdir(resultpath) # Going to the result directory
ecutvslatfile = open(outfilename1,'w') # Create the result file
for j in range(len(ecut_list)): # To write all the data into the file
    ecutvslatfile.write('{0:<8d} {1:20.9f}\n'.format(int(ecut_list[j]),float(final_lat_param[j] )))
ecutvslatfile.close() # Closing the file

print('\n\n------------------------------------')
print('\nInput from {0}'.format(filepath))
print('Results in {0}\n'.format(resultpath))
print('------------------------------------')
