"""
This file corresponds to all the functions used in the software
"""

"""
MODULES IMPORTATION
"""

import sys
import os
import re
import numpy as np
import math

"""
FUNCTIONS
"""

def patternline3_2psqrt2(file,regexpress):
    """
    This function return and write the three line after the pattern from a
    .out file (Quantum espresso) as a shape of a 3x3 matrix. The first row
    corresponds to the a direction, the second the b direction and the last
    the c direction. It is written for psqrt2 fcc lattice cell
    """
    outfilelines = file.readlines()
    a = []
    b = []
    c = []
    j=0
    i=0
    match = False
    for line in outfilelines:
        if j != 0:
            if j == 1:
                x = line.split()
                a.append(resize_psart2(float(x[0])))
                # print ("x : " + a[0])
            elif j == 2:
                y = line.split()
                b.append(resize_psart2(float(y[1])))
            elif j ==3:
                z = line.split()
                c.append(float(z[2]))
                # print ("z : " + z[2])
            j += 1
        if j == 4:
            j=0
        if re.search(regexpress,line):
            match = True
            i += 1
            j = 0
        if match == True:
            j+=1
            match = False
    matrix = np.array([a,b,c])
    print(matrix)
    return matrix

def patternline3_fcc(file,regexpress):
    """
    This function return and write the three line after the pattern from a
    .out file (Quantum espresso) as a shape of a 3x3 matrix. The first row
    corresponds to the a direction, the second the b direction and the last
    the c direction. It is written for fcc lattice cells
    """
    outfilelines = file.readlines()
    a = []
    b = []
    c = []
    j=0
    i=0
    match = False
    for line in outfilelines:
        if re.search("lattice parameter",line):
            latvalue = float(line.split()[4])
        if j != 0:
            if j == 1:
                x = line.split()
                a.append(float(x[0]))
                # print ("x : " + a[0])
            elif j == 2:
                y = line.split()
                b.append(float(y[1]))
            elif j ==3:
                z = line.split()
                c.append(float(z[2]))
                # print ("z : " + z[2])
            j += 1
        if j == 4:
            j=0
        if re.search(regexpress,line):
            match = True
            i += 1
            j = 0
        if match == True:
            j+=1
            match = False
    matrix = np.array([a,b,c]) * latvalue
    return matrix

def files(path):
    """
    This function returns only the name of files from a directory as a list.
    """
    list = []
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            list.append(file)

    return sorted(list)

def resize_psart2(latparameter):
    """
    This function resize the cell parameter from a p2sqrt2x2sqrt2x1 fcc lattice
    cell
    """
    latresized = (latparameter*2.0)/math.sqrt(2)
    return latresized

def renameres(filename):
    "This function rename a .* file into a .res file"
    resfilename1 = filename.split(".")
    resfilename2 = resfilename1[0] + ".res"
    return resfilename2

def nameresfile(filename,resfilename,separator="_",nsep=1):
    """This function create a name for the result's file. It keeps the last value
    before the .out to write the resfile. The separator used by default is after
    the first "_" in the filename.
    """
    resfilename1 = filename.split(".")
    resfilename2 = resfilename1[0].split(separator)
    resfilename3 = str(resfilename) + resfilename2[int(nsep)] + ".res"
    return resfilename3

def valuefromfilename(filename,separator="_",nsep=1):
    """ This function takes value from the filename as the first string after a
    "_". This value can be changed by the value nsep. The separator can be changed
    by tunning the value separator> The value is by default a float.
    """
    resfilename1 = filename.split(".")
    resfilename2 = resfilename1[0].split(separator)
    value = resfilename2[int(nsep)]
    return value

def angstobohr(angs):
    """This function convert angstroms into bohrt
    """
    convfact = 1.0/0.529177249
    bohr = angs * convfact
    return bohr

def bohrtoangs(bohr):
    """ This function convert bohr into angstroms
    """
    convfact = 0.529177249
    angstrom = bohr * convfact
    return angstrom

def ibrav(bravaislat,a,nrepa,nrepb,nlayer,atnam1,atnam2):
    """
    This function returns a transformation matrix according to the wanted
    bravais structure
    """
    if bravaislat == 'fcc':
        print("Bravais lattice: fcc")
        print("The lattice parameter is: {0:<10.8f} Å".format(a))
        #
        nbatoms = (1+nrepa)*(1+nrepb)*nlayer
        print("The total number of atoms is {0:d}\n".format(nbatoms))
        # matrix = np.zeros( (nbatoms,4) )
        # matrix = (5, dtype={'atoms':('atom', 'x', 'y', 'z'),
                          # 'formats':('U10', 'f10', 'f10', 'f10')})
        atomlist = []
        xlist = []
        ylist = []
        zlist = []
        xmax = nrepa * 0.5 * a
        ymax = nrepb * 0.5 * a
        zmax = nlayer * 0.5 * a
        xdist = 0
        ydist=0
        zdist=0
        j = 0
        begatm1ini = True
        begatm1 = True
        # Loop over all the matrix/atoms
        for i in range(nbatoms):
            # Construction of the x line of the surface
            if xdist <= xmax:
                # Define the name of the atoms
                j+=1
                if (j % 2) !=0 and (begatm1) ==True:
                    atomlist.append(atnam1)
                elif (j % 2) !=0 and (begatm1) ==False:
                    atomlist.append(atnam2)
                elif (j % 2) ==0 and (begatm1) ==True:
                    atomlist.append(atnam2)
                elif (j % 2) ==0 and (begatm1) ==False:
                    atomlist.append(atnam1)
                # Atom list initialization
                xlist.append(xdist)
                ylist.append(ydist)
                zlist.append(zdist)
                xdist += a * 0.5
                # Adding a new y line to the surface
                if xdist > xmax:
                    j = 0
                    if (begatm1) ==True:
                        begatm1 = False
                    elif (begatm1) ==False:
                        begatm1 = True
                    xdist = 0
                    ydist += a * 0.5
                    # Adding layers to the solid
                    if ydist > ymax:
                        if (begatm1ini) ==True:
                            begatm1ini = False
                            begatm1 = begatm1ini
                        elif (begatm1ini) ==False:
                            begatm1ini = True
                            begatm1 = begatm1ini
                        ydist = 0
                        zdist += a * 0.5
        matrix = np.zeros(nbatoms, dtype={'names':('atom', 'x', 'y', 'z'),
                          'formats':('U10', 'f8', 'f8', 'f8')})
        matrix['atom'] = atomlist
        matrix['x'] = xlist
        matrix['y'] = ylist
        matrix['z'] = zlist
        return matrix
    else: #Stopping the program if the bravais lattice does not exist
        print("The bravais lattice {0} does not exist in this program".format(bravaislat))
        print("The program has to stop")
        print('\n---------------------------------END')
        exit()
